import { Button, Checkbox, FormControl, FormControlLabel, Grid, MenuItem, TextField } from '@mui/material';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { bikeEnumType } from '../../services/enumType';
import { addVehicle } from '../../services/vehicle-service';

const initialState = {
    
    brand: '',
    color: '',
    purchaseDate: '',
    purchasePrice: null,
    sellingPrice: null,
    vehicleType: 'BIKE',
    chainRings: null,
    isMarked: false,
    bikeType: ''
}

export default function FormBike() {

    const [bikeValues, setBikeValues] = useState(initialState);
    const [response, setResponse] = useState(null);
    const navigate = useNavigate();

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setBikeValues({
            ...bikeValues,
            [name]: value,
        });
    }

    const handleCheckboxChange = (e) => {
        setBikeValues({
            ...bikeValues,
            [e.target.name]: e.target.checked
        })
    }
   
    const handleSubmit = event => {
        event.preventDefault();
        console.log(bikeValues);
        addVehicle(bikeValues).then(data => setResponse(data));
        event.target.reset()
        navigate(`/`);

    }

  return (
    <>
        <form onSubmit={handleSubmit}>
            <Grid 
                container
                spacing={3}
                sx={{
                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                }}
            >
                 <Grid item md={12}>
                    <h2>Add a new Bike</h2>
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='brand'
                        name='brand'
                        label='Brand'
                        type='text'
                        value={bikeValues.brand}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='color'
                        name='color'
                        label='Color'
                        type='text'
                        value={bikeValues.color}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='purchaseDate'
                        name='purchaseDate'
                        label='Purchase date'
                        type='date'
                        value={bikeValues.purchaseDate}
                        onChange={handleInputChange}
                        InputLabelProps={{ shrink: true }}
                        required
                    />
                    
                </Grid>
                <Grid item md={6}>
                    <TextField
                        id='purchasePrice'
                        name='purchasePrice'
                        label='Purchase price'
                        type='number'
                        value={bikeValues.purchasePrice}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                
                <Grid item md={6}>
                        <TextField
                        id='bikeType'
                        name="bikeType"
                        label='Type'
                        select
                        value={bikeValues.bikeType}
                        onChange={handleInputChange}
                        required
                        >
                            {Object.keys(bikeEnumType).map((key)=>(
                                <MenuItem key={key} value={key}>
                                {bikeEnumType[key]}
                            </MenuItem>
                            ))}
                        
                        </TextField>
                </Grid>
                <Grid item md={6}>
                    <FormControl>
                        <FormControlLabel
                            control={
                                <Checkbox 
                                    name='isMarked'
                                    checked={bikeValues.isMarked} 
                                    onChange={handleCheckboxChange} 
                                />
                            }
                            label="Bike is marked"
                        />
                    </FormControl>
                    
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='chainRings'
                        name='chainRings'
                        label='Number of chain rings'
                        type='number'
                        value={bikeValues.chainRings}
                        onChange={handleInputChange}
                        required
                    />
                    
                </Grid>

                <Grid item md={6}>
                    <TextField
                        id='sellingPrice'
                        name='sellingPrice'
                        label='Selling price'
                        type='number'
                        value={bikeValues.sellingPrice}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={12}>
                    <Button
                        variant='contained'
                        color='primary'
                        type='submit'
                    >
                        Submit
                    </Button>
                </Grid>

                
            </Grid>
        </form>
    </>
  )
}
