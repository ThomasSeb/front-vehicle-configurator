import { Grid, MenuItem, TextField } from '@mui/material'
import React, { useState } from 'react'
import { vehicleEnumType } from '../../services/enumType'
import FormBike from './FormBike'
import FormCar from './FormCar'
import FormMoto from './FormMoto'

export default function FormVehicle() {
    const [vehicleType, setVehicleType] = useState('CAR')
    
    const handleInputChange = (e) => {
        setVehicleType(e.target.value)
    }

  return (
    
     <Grid 
        container
        spacing={3}
        sx={{
            '& .MuiTextField-root': { m: 1, width: '50ch' },
        }}
    >
        <Grid item md={12}>
            <TextField
            id='vehicleType'
            name="vehicleType"
            label='What type of vehicle would you create ?'
            select
            value={vehicleType}
            onChange={handleInputChange}
            >
                {Object.keys(vehicleEnumType).map((key)=>(
                    <MenuItem key={key} value={key}>
                    {key}
                </MenuItem>
                ))}
            
            </TextField>
        </Grid>
        {vehicleType === 'CAR' ?
            <FormCar />
        : vehicleType === 'MOTORCYCLE' ?
            <FormMoto />
        : vehicleType === 'BIKE' ?
            <FormBike />
        : null
        }
    </Grid>
   
  )
}
