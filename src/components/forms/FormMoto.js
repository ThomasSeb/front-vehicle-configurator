import { Button, Grid, MenuItem, TextField } from '@mui/material';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { motorcyleEnumType } from '../../services/enumType';
import { addVehicle } from '../../services/vehicle-service';

const initialState = {
    
    brand: '',
    color: '',
    purchaseDate: '',
    purchasePrice: 0,
    sellingPrice: 0,
    vehicleType: 'MOTORCYCLE',
    kilometers: 0,
    firstRegistration: '',
    horsePower: 0,
    cubicCapacity: 0,
    motoType: ''
        
}

export default function FormMoto() {

    const [motoValues, setMotoValues] = useState(initialState);
    const [response, setResponse] = useState(null);
    const navigate = useNavigate();

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setMotoValues({
            ...motoValues,
            [name]: value,
        });
    }

   
    const handleSubmit = event => {
        event.preventDefault();
        console.log(motoValues);
        addVehicle(motoValues).then(data => setResponse(data));
        event.target.reset()
        navigate(`/`);
    }

  return (
    <>
        
        <form onSubmit={handleSubmit}>

            <Grid 
                container
                spacing={3}
                sx={{
                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                }}
            >
                <Grid item md={12}>
                    <h2>Add a new Moto</h2>
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='brand'
                        name='brand'
                        label='Brand'
                        type='text'
                        value={motoValues.brand}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='color'
                        name='color'
                        label='Color'
                        type='text'
                        value={motoValues.color}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='purchaseDate'
                        name='purchaseDate'
                        label='Purchase date'
                        type='date'
                        value={motoValues.purchaseDate}
                        onChange={handleInputChange}
                        InputLabelProps={{ shrink: true }}
                        required
                    />
                    
                </Grid>
                <Grid item md={6}>
                    <TextField
                        id='purchasePrice'
                        name='purchasePrice'
                        label='Purchase price'
                        type='number'
                        value={motoValues.purchasePrice}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='kilometers'
                        name='kilometers'
                        label='Kilometers'
                        type='number'
                        value={motoValues.kilometers}
                        onChange={handleInputChange}
                        required
                    />
                    
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='firstRegistration'
                        name='firstRegistration'
                        label='First registration date'
                        type='date'
                        value={motoValues.firstRegistration}
                        onChange={handleInputChange}
                        InputLabelProps={{ shrink: true }}
                        required
                    />
                    
                </Grid>
                <Grid item md={6}>
                        <TextField
                        id='motoType'
                        name="motoType"
                        label='Type'
                        select
                        value={motoValues.motoType}
                        onChange={handleInputChange}
                        required
                        >
                            {Object.keys(motorcyleEnumType).map((key)=>(
                                <MenuItem key={key} value={key}>
                                {motorcyleEnumType[key]}
                            </MenuItem>
                            ))}
                        
                        </TextField>
                </Grid>
                
                <Grid item md={6}>
                    <TextField 
                        id='cubicCapacity'
                        name='cubicCapacity'
                        label='Cubic capacity'
                        type='number'
                        value={motoValues.cubicCapacity}
                        onChange={handleInputChange}
                        required
                    />
                    
                </Grid>

                <Grid item md={6}>
                    <TextField 
                        id='horsePower'
                        name='horsePower'
                        label='Horsepower'
                        type='number'
                        value={motoValues.horsePower}
                        onChange={handleInputChange}
                        required
                    />
                    
                </Grid>
                <Grid item md={6}>
                    <TextField
                        id='sellingPrice'
                        name='sellingPrice'
                        label='Selling price'
                        type='number'
                        value={motoValues.sellingPrice}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={12}>
                    <Button
                        variant='contained'
                        color='primary'
                        type='submit'
                    >
                        Submit
                    </Button>
                </Grid>

                
            </Grid>
        </form>
    </>
  )
}
