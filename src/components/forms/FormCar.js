import { Button, Checkbox, FormControl, FormControlLabel, Grid, MenuItem, Select, TextField } from '@mui/material';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { carEnumType } from '../../services/enumType';
import { addVehicle } from '../../services/vehicle-service';

const initialState = {
    
    brand: '',
    color: '',
    purchaseDate: '',
    purchasePrice: 0,
    sellingPrice: 0,
    vehicleType: 'CAR',
    kilometers: 0,
    firstRegistration: '',
    carType: '',
    doorNbr: 0,
    isAutomatic: false,
    horsePower: 0
}

export default function FormCar() {

    const [carValues, setCarValues] = useState(initialState);
    const [response, setResponse] = useState(null);
    const navigate = useNavigate();

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setCarValues({
            ...carValues,
            [name]: value,
        });
    }

    const handleCheckboxChange = (e) => {
        setCarValues({
            ...carValues,
            [e.target.name]: e.target.checked
        })
    }
   
    const handleSubmit = event => {
        event.preventDefault();
        console.log(carValues);
        addVehicle(carValues).then(data => setResponse(data));
        event.target.reset()
        navigate(`/`);
    }

  return (
    <>
        <form onSubmit={handleSubmit}>
            <Grid 
                container
                spacing={3}
                sx={{
                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                }}
            >
                <Grid item md={12}>
                    <h2>Add a new Car</h2>
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='brand'
                        name='brand'
                        label='Brand'
                        type='text'
                        value={carValues.brand}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='color'
                        name='color'
                        label='Color'
                        type='text'
                        value={carValues.color}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='purchaseDate'
                        name='purchaseDate'
                        label='Purchase date'
                        type='date'
                        value={carValues.purchaseDate}
                        onChange={handleInputChange}
                        InputLabelProps={{ shrink: true }}
                        required
                    />
                    
                </Grid>
                <Grid item md={6}>
                    <TextField
                        id='purchasePrice'
                        name='purchasePrice'
                        label='Purchase price'
                        type='number'
                        value={carValues.purchasePrice}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='kilometers'
                        name='kilometers'
                        label='Kilometers'
                        type='number'
                        value={carValues.kilometers}
                        onChange={handleInputChange}
                        required
                    />
                    
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='firstRegistration'
                        name='firstRegistration'
                        label='First registration date'
                        type='date'
                        value={carValues.firstRegistration}
                        onChange={handleInputChange}
                        InputLabelProps={{ shrink: true }}
                        required
                    />
                    
                </Grid>
                <Grid item md={6}>
                        <TextField
                        id='carType'
                        name="carType"
                        label='Type'
                        select
                        value={carValues.carType}
                        onChange={handleInputChange}
                        required
                        >
                            {Object.keys(carEnumType).map((key)=>(
                                <MenuItem key={key} value={key}>
                                {carEnumType[key]}
                            </MenuItem>
                            ))}
                        
                        </TextField>
                </Grid>
                <Grid item md={6}>
                    <FormControl>
                        <FormControlLabel
                            control={
                                <Checkbox 
                                    name='isAutomatic'
                                    checked={carValues.isAutomatic} 
                                    onChange={handleCheckboxChange} 
                                />
                            }
                            label="Automatic transmission"
                        />
                    </FormControl>
                    
                </Grid>
                <Grid item md={6}>
                    <TextField 
                        id='doorNbr'
                        name='doorNbr'
                        label='Number of doors'
                        type='number'
                        value={carValues.doorNbr}
                        onChange={handleInputChange}
                        required
                    />
                    
                </Grid>

                <Grid item md={6}>
                    <TextField 
                        id='horsePower'
                        name='horsePower'
                        label='Horsepower'
                        type='number'
                        value={carValues.horsePower}
                        onChange={handleInputChange}
                        required
                    />
                    
                </Grid>
                <Grid item md={6}>
                    <TextField
                        id='sellingPrice'
                        name='sellingPrice'
                        label='Selling price'
                        type='number'
                        value={carValues.sellingPrice}
                        onChange={handleInputChange}
                        required
                    />
                </Grid>
                <Grid item md={12}>
                    <Button
                        variant='contained'
                        color='primary'
                        type='submit'
                    >
                        Submit
                    </Button>
                </Grid>

                
            </Grid>
        </form>
    </>
  )
}
