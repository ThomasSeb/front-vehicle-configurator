import { Button, Card, CardContent, Grid, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import { deleteVehicle, findVehicle } from '../services/vehicle-service';

export const OneVehicle = () => {
    const {id, vehicleType} = useParams();
    const [vehicle, setVehicle] = useState(null);
    let navigate = useNavigate();

    useEffect(() => {
        findVehicle(id, vehicleType).then(vehicle => setVehicle(vehicle))
    }, [id, vehicleType])

    const deleteVeh = id => {
        deleteVehicle(id);
        navigate("/")
    };

  return (
    <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
    >
        {vehicle ? 
        <>
            <h2>{vehicle.vehicleType}</h2>
            <Card sx={{minWidth: 500}}>
                <CardContent>
                    <Typography>
                        Brand: {vehicle.brand}
                    </Typography>
                    <Typography>
                        Color: {vehicle.color}
                    </Typography>
                    <Typography>
                        Purchase date: {vehicle.purchaseDate}
                    </Typography>
                    <Typography>
                        Purchase price: {vehicle.purchasePrice} €
                    </Typography>
                    {vehicle.vehicleType === "MOTORCYCLE" &&
                        <>
                            <Typography>
                                Kilometers: {vehicle.kilometers}
                            </Typography>
                            <Typography>
                                First registration date: {vehicle.firstRegistration}
                            </Typography>
                            <Typography>
                                Horsepower: {vehicle.horsepower}
                            </Typography>
                            <Typography>
                                Cubic capacity: {vehicle.cubicCapacity}
                            </Typography>
                            <Typography>
                                Type: {vehicle.motoType}
                            </Typography>
                        </>
                    
                    }
                    {vehicle.vehicleType === "CAR" &&
                        <>
                            <Typography>
                                Kilometers: {vehicle.kilometers}
                            </Typography>
                            <Typography>
                                First registration date: {vehicle.firstRegistration}
                            </Typography>
                            <Typography>
                                Horsepower: {vehicle.horsePower}
                            </Typography>
                            <Typography>
                                Doors: {vehicle.doorNbr}
                            </Typography>
                            <Typography>
                                Automatic: {vehicle.isAutomatic ? "Yes" : "No"}
                            </Typography>
                            <Typography>
                                Type: {vehicle.carType}
                            </Typography>
                        </>
                    }
                    {vehicle.vehicleType === "BIKE" &&
                        <>
                            <Typography>
                                Chain rings: {vehicle.chainRings}
                            </Typography>
                            <Typography>
                                Marked: {vehicle.isMarked ? "Yes" : "No"}
                            </Typography>
                            <Typography>
                                Type: {vehicle.bikeType}
                            </Typography>
                        </>
                    }
                    <Typography>
                        Selling price: {vehicle.sellingPrice} €
                    </Typography>
                </CardContent>
            </Card>
            <Button 
                variant='contained' 
                color='error'
                onClick={() => deleteVeh(vehicle.id)}
            >
                Delete
            </Button>
        </> 
         : <p>loading....</p>}
    </Grid>
  )
}
