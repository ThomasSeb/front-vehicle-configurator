import React, { useEffect, useState } from 'react';
import { Button, Grid, Paper, styled, Table, TableBody, TableCell, tableCellClasses, TableHead, TableRow } from '@mui/material';
import TableContainer from '@mui/material/Table';
import { Link } from 'react-router-dom';
import { fetchVehicles } from '../services/vehicle-service';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

export default function ListVehicle(){

  const [vehicles, setVechicles] = useState([]);

    useEffect(() => {
        fetchVehicles().then(data => setVechicles(data));
    }, [vehicles]);
    
    return (
      <>
        <TableContainer component={Paper}>
            <Table sx={{minWidth: 650}} aria-label="vehicle table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell align='center'>Vehicle Type</StyledTableCell>
                        <StyledTableCell align='center'>Brand</StyledTableCell>
                        <StyledTableCell align='center'>Purchase Price</StyledTableCell>
                        <StyledTableCell align='center'>Selling Price</StyledTableCell>
                        <StyledTableCell align='center'>Purchase Date</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {vehicles.map(vehicle => (
                        <StyledTableRow 
                            key={vehicle.id} 
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            component={Link} to={`/vehicle/${vehicle.vehicleType}/${vehicle.id}`}
                        >
                            <StyledTableCell align='center' component="th" scope="row">{vehicle.vehicleType}</StyledTableCell>
                            <StyledTableCell align='center'>{vehicle.brand}</StyledTableCell>
                            <StyledTableCell align='center'>{vehicle.purchasePrice} €</StyledTableCell>
                            <StyledTableCell align='center'>{vehicle.sellingPrice} €</StyledTableCell>
                            <StyledTableCell align='center'>{vehicle.purchaseDate}</StyledTableCell>
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>



        </TableContainer>
        <Grid 
        container
        spacing={3}
        sx={{
            '& .MuiTextField-root': { m: 1, width: '50ch' },
        }}
        >
        <Grid item md={12}>
          <Button
              variant='contained'
              color='primary'
              href='/vehicle/add'
          >
              Add a vehicle
          </Button>
        </Grid>
        </Grid>
    </>
        // <ul>
        //     {vehicles.map(vehicle => (
        //         <li key={vehicle.id}>
        //             <p>{vehicle.brand} {vehicle.vehicleType} </p>
        //         </li>
        //     ))}
        // </ul>
    )
}