import './App.css';
import { Route, Routes } from 'react-router-dom';
import { OneVehicle } from './components/OneVehicle';
import ListVehicle from './components/ListVehicle';
import FormVehicle from './components/forms/FormVehicle';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<ListVehicle/>} />
        <Route path="/vehicle/:vehicleType/:id" element={<OneVehicle />} />
        <Route path="/vehicle/add" element={<FormVehicle />} />
      </Routes>
           
    </div>
  );
}

export default App;
