export const vehicleEnumType = {
    CAR: "CAR",
    MOTORCYCLE: "MOTORCYCLE",
    BIKE: "BIKE"
    
}

export const carEnumType = {
    SEDAN:"Sedan",
    COUPE:"Coupe",
    WAGON:"Wagon",
    CONVERTIBLE:"Convertible",
    PICKUP:"Pick-up",
    ALLTERRAIN:"All-Terrain",
    SUV:"SUV",
    SPORTS:"Sports"
}

export const motorcyleEnumType = {
    ROAD: "Road",
    ROADSTER: "Roadster",
    SPORTS: "Sports",
    SUPERMOTO: "Supermoto",
    CUSTOM: "Custom"
}

export const bikeEnumType = {
    MOUNTAIN: "Mountain",
    ROAD: "Road",
    RACING: "Racing"
}