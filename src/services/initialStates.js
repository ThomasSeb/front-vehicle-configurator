
export const initialCarState = {
    vehicle: {
        brand: '',
        color: '',
        purchaseDate: '',
        purchasePrice: 0,
        sellingPrice: 0,
        vehicleType: ''
    },
    kilometers: 0,
    firstRegistration: '',
    carType: '',
    doorNbr: 0,
    isAutomatic: false,
    horsePower: 0
}

export const initialMotoState = {
    vehicle: {
        brand: '',
        color: '',
        purchaseDate: '',
        purchasePrice: 0,
        sellingPrice: 0,
        vehicleType: ''
    },
    kilometers: 0,
    firstRegistration: '',
    horsePower: 0,
    cubicCapacity: 0,
    motoType: '',
    isAutomatic: false, 
}

export const initialBikeState = {
    vehicle: {
        brand: '',
        color: '',
        purchaseDate: '',
        purchasePrice: 0,
        sellingPrice: 0,
        vehicleType: ''
    },
    chainRings: 0,
    isAutomatic: false,
    isMarked: false 
}