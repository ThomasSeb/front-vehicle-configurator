import axios from 'axios';

const url = 'http://localhost:8080/api/vehicle';

export async function fetchVehicles() {
    const response = await axios.get(url);
    return response.data;
}

export async function findVehicle(id, vehicleType) {
    const response = await axios.get(url + '/' + vehicleType + '/' + id);
    return response.data;
}

export async function addVehicle(vehicle) {
    const response = await axios.post(url + '/add', vehicle);
    return response.data;
}

export async function deleteVehicle(id){
    
    await axios.delete(url + '/' + id);
    return true;
}
